﻿using BookmarkManager.Library.Commands;
using BookmarkManager.Library.Commands.Abstract;
using System.Collections.ObjectModel;

namespace BookmarkManager.CLI
{
    class Program
    {
        static void Main(string[] args)
        {
            var options = new ReadOnlyCollection<ICommand>(new ICommand[]
            {
                new AddBookmarkCommand(),
                new ShowAllBookmarksCommand(),
                new OpenBookmarkCommand(),
                new ExitCommand()
            });

            new Menu(options).Run();
        }
    }
}
