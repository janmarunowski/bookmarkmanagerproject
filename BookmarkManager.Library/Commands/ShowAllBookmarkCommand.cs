﻿using System;
using BookmarkManager.Library.Commands.Abstract;
using BookmarkManager.Library.Service;

namespace BookmarkManager.Library.Commands
{
    public class ShowAllBookmarksCommand : ICommand
    {
        public string Description => "Wyświetl wszystkie zapisane zakładki.";

        public void Execute()
        {
            var bookmarkService = new BookmarkService();
            var bookMarks = bookmarkService.GetBookmarks();

            Console.WriteLine("WSZYSTKIE ZAKŁADKI: ");

            foreach (var item in bookMarks)
            {
                Console.WriteLine($"Nazwa zakładki - {item.BookmarkName}, URL zakładki - {item.Url}");
            }

            Console.WriteLine("Naciśnij enter, aby powrócić do menu.");
        }
    }
}
