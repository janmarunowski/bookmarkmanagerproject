﻿using BookmarkManager.Library.Commands.Abstract;
using BookmarkManager.Library.Service;
using System;
using System.Linq;

namespace BookmarkManager.Library.Commands
{
    public class OpenBookmarkCommand : ICommand
    {
        public string Description => "Otwórz zakładkę.";
        public void Execute()
        {
            var bookmarkService = new BookmarkService();
            var bookMarks = bookmarkService.GetBookmarks();
            foreach (var item in bookMarks)
            {
                Console.WriteLine($"Nazwa zakładki - {item.BookmarkName}, URL zakładki - {item.Url}");
            }

            var consoleOperator = new ConsoleOperator();
            consoleOperator.WriteLine("Wybierz zakładkę, którą chcesz otworzyć w przeglądarce (podaj nazwę tej zakładki).");

            var bookmarkUrlInput = consoleOperator.Read();
            var selectedBookMark = bookMarks.FirstOrDefault(b => b.BookmarkName == bookmarkUrlInput)?.Url;

            System.Diagnostics.Process.Start("cmd", $"/C start {selectedBookMark}");

            Console.WriteLine("Naciśnij enter, aby powrócić do menu.");
        }
    }
}
