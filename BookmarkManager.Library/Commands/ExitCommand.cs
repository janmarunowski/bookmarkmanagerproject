﻿using BookmarkManager.Library.Commands.Abstract;
using System;

namespace BookmarkManager.Library.Commands
{
    public class ExitCommand : ICommand
    {
        public string Description => "Wyjście.";
        public void Execute()
        {
            Environment.Exit(0);
        }
    }
}
