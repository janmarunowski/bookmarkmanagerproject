﻿using BookmarkManager.Library.Commands.Abstract;
using BookmarkManager.Library.Models;
using BookmarkManager.Library.Service;
using System;

namespace BookmarkManager.Library.Commands
{
    public class AddBookmarkCommand : ICommand
    {
        public string Description => "Dodaj zakładkę.";
        public void Execute()
        {
            var consoleOperator = new ConsoleOperator();

            consoleOperator.WriteLine("Podaj nazwę zakładki: ");
            var bookmarkInput = consoleOperator.Read();

            consoleOperator.WriteLine("Wprowadź URL: ");
            var urlInput = consoleOperator.Read();

            var isUrl = Uri.TryCreate(urlInput, UriKind.Absolute, out var url);

            if (!isUrl || string.IsNullOrWhiteSpace(bookmarkInput) || string.IsNullOrWhiteSpace(urlInput))
            {
                consoleOperator.WriteLine("Błędny format URL! (skopiuj link zakładki z przeglądarki");
            }

            var bookmarkService = new BookmarkService();
            var bookMarks = bookmarkService.GetBookmarks();
            bookMarks.Add(new Bookmark(bookmarkInput, url));
            bookmarkService.SaveBookmarks(bookMarks);

            Console.WriteLine("Zakładka została pomyślnie zapisana. Naciśnij enter, aby powrócić do menu.");
        }
    }
}

