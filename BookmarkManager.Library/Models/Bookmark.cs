﻿using System;

namespace BookmarkManager.Library.Models
{
    public class Bookmark
    {
        public Bookmark(string bookmarkName, Uri url)
        {
            BookmarkName = bookmarkName;
            Url = url;
        }
        public string BookmarkName { get; }
        public Uri Url { get; }
    }
}
