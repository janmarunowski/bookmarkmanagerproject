﻿using BookmarkManager.Library.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;

namespace BookmarkManager.Library.Service
{
    public class BookmarkService
    {
        public List<Bookmark> GetBookmarks()
        {
            var pathCombine = AppSettings.FilePath;

            if (!File.Exists(pathCombine))
            {
                File.Create(pathCombine);
            }

            var readText = File.ReadAllText(pathCombine);
            if (!string.IsNullOrEmpty(readText))
            {
                try
                {
                    return JsonConvert.DeserializeObject<List<Bookmark>>(readText);
                }
                catch (Exception)
                {
                    Console.WriteLine("Usunę cały plik, ponieważ nastąpiła jakaś zmiana zewnętrzna.");
                    File.Delete(pathCombine);
                }
            }
            return new List<Bookmark>();
        }
        public void SaveBookmarks(List<Bookmark> bookMarks)
        {
            var pathCombine = AppSettings.FilePath;
            var bookmarkJson = JsonConvert.SerializeObject(bookMarks);

            using (StreamWriter streamWriter = new StreamWriter(pathCombine))
            {
                streamWriter.WriteLine(bookmarkJson);
            }
        }
    }
}
