﻿using System;

namespace BookmarkManager.Library
{
    public class ConsoleOperator
    {
        public void WriteLine(string value)
        {
            Console.WriteLine(value);
        }
        public string Read()
        {
            return Console.ReadLine();
        }
    }
}
