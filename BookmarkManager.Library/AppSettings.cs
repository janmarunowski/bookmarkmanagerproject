﻿using System.IO;

namespace BookmarkManager.Library
{
    public class AppSettings
    {
        private const string FileName = "url.txt";
        public static string FilePath => Path.Combine(Directory.GetCurrentDirectory(), FileName);
    }
}
